#Using git for steps

# Download remote project:
-> git clone https...

# Push project to remote/ live server:
-> git add . 			  #Take all dev files to staging area.
-> git commit -m "[your message]" #Push to server with unit message.
-> git push 			  #Originally pushing to server.