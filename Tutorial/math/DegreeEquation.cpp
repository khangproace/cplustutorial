#include <iostream>
#include <cmath>
using namespace  std;
int main() {
    float a;
    float b;
    float c;
    float x1;
    float x2;
    float delta;

    cout << "Input a: ";
    cin >> a;
    while (a == 0) {
        cout << "Input a: ";
        cin >> a;
    }

    cout << "Input b: ";
    cin >> b;
    cout << "Input c: ";
    cin >> c;
    cout << "Your equation is: " << (a) << "x^2 + " << (b) << "x + " << (c) << " = 0" << endl;

    delta = pow(b, 2) - 4 * ((a) * (c));

    if (delta < 0) {
        cout << "No solution";
    } else if (delta == 0) {
        //TODO
        x1 = -b / (2*a);
        cout << "x1 = " << (x1);
    } else {
        //TODO
        x1 = (-b + sqrt(delta)) / (2 * a);
        x2 = (-b - sqrt(delta)) / (2 * a);
        cout << "x1 = " << (x1) << endl;
        cout << "x2 = " << (x2);
    }


    return 0;
}