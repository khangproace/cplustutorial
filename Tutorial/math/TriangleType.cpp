#include <iostream>
#define MAX 10000
#include<math.h>
using namespace std;

int main () {
    int firstSide = 0;
    int secondSide = 0;
    int thirdSide = 0;

    cout << "First side = ";
    cin >> firstSide;
    cout << "Second side = ";
    cin >> secondSide;
    cout << "Third side = ";
    cin >> thirdSide;

    if (firstSide + secondSide <= thirdSide || secondSide + thirdSide <= firstSide || firstSide + thirdSide <= secondSide) {
        cout << "Error";
    }

    else if (firstSide == secondSide && firstSide == thirdSide) {
        cout << "This is an equilateral triangle.";
    }

    else if ((firstSide == secondSide and firstSide != thirdSide) || (secondSide == thirdSide and secondSide != firstSide)
        || (firstSide == thirdSide and firstSide != secondSide)) {
        cout << "This is an isoceles triangle.";
    }

    else if ( pow(firstSide, 2) + pow(secondSide , 2) == pow(thirdSide, 2) || pow(firstSide, 2) + pow(thirdSide, 2) == pow(secondSide , 2)
        || pow(secondSide , 2) + pow(thirdSide, 2) == pow(firstSide, 2) ){
        cout << "This is a right triangle.";
    }
    else {
        cout << "This is a normal triangle.";
    }



    return 0;
}