#include <iostream>
using namespace std;

int main () {
    int startIndex;
    int endIndex;

    cout << "Enter startIndex: ";
    cin >> startIndex;
    cout << "Enter endIndex: ";
    cin >> endIndex;

    if (startIndex < 0) {
        startIndex = 0;
    }
    if (endIndex > 255) {
        endIndex = 255;
    }

    while (startIndex <= endIndex){
        cout << char (startIndex) << " ";
        startIndex += 1;
    }




    return 0;
}