#include <iostream>
using namespace std;

struct Node {
    int data;
    Node *pNext;
};

Node *createNode (int init) {
    Node *node = new Node;
    node->data = init;
    node->pNext = nullptr;
    return node;
};

struct Queue {
    Node *head;
    Node *tail;
};

Queue createQueue (Queue &s) {
    s.head = nullptr;
    s.tail = nullptr;
};

bool isEmpty (Queue s) {
    return (s.head == nullptr && s.tail == nullptr )? true : false;
};

void enQueue (Queue &s, Node *node) {
    if (isEmpty(s)) {
        s.head = node;
        s.tail = node;
    }
    else {
        s.tail->pNext = node;
        s.tail = node;
    }
};

int deQueue (Queue &s) {
    if (isEmpty(s)) {
        return 0;
    }
    Node *node = s.head;
    int data = node->data;
    s.head = node->pNext;
    delete node;
    if (s.head == nullptr) {
        s.tail = nullptr;
    };
    return data;
}

;
int main () {
    Queue queue1;
    createQueue(queue1);

    Node *node1 = createNode(1);
    Node *node2 = createNode(2);
    Node *node3 = createNode(3);

    enQueue(queue1, node1);
    enQueue(queue1, node2);
    enQueue(queue1, node3);

    deQueue(queue1);

    cout << queue1.head->data;





    return 0;
}