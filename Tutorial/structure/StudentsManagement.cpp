#include <iostream>
#define MAX 100000
using namespace std;

// Object
typedef struct StudentInformation {
    //Property
    int dOB;
    string name;
    string address;
    float score;
};

StudentInformation create(int dOB, string name, string address, float score) {
    StudentInformation student;
    student.dOB = dOB;
    student.address = address;
    student.name = name;
    student.score = score;
    return student;
}

void print(StudentInformation student) {
    cout << student.score;
    cout << " | ";
    cout << student.name;
    cout << " | ";
    cout << student.dOB;
    cout << " | ";
    cout << student.address << endl;
}

//StudentInformation compare(StudentInformation student1, StudentInformation student2) {
//        if (student1.score > student2.score) {
//            return student1;
//        } else if (student2.score > student1.score) {
//        } else {
//            cout << "They have the same score";
//            return;
//        }
//}

StudentInformation* selectionSort (StudentInformation list[]) {
    StudentInformation *pointer;
    int j;
    int e;
    int min;
    for (e = 0; e < 4 - 1; e++) {
        min = e + 1;
        for (j = e + 1; j < 4; j++) {
            if (list[min].score < list[j].score) {
                min = j;
            }
        }
        if (list[e].score < list[min].score) {
            StudentInformation t = list[e];
            list[e] = list[min];
            list[min] = t;
        }
    };

    pointer = list;
    return pointer;
}

StudentInformation* bubbleSort (StudentInformation list[]) {
    StudentInformation* pointer;
    float e;
    int j;
    StudentInformation t;
    bool flag;
    for (e = 0; e < 4 - 1; e++) {
        flag = false;
        for (j = 0; j < 4 - e - 1; j++) {
            if (list[j].score < list[j + 1].score) {
                t = list[j + 1];
                list[j + 1] = list[j];
                list[j] = t;
                flag = true;
            }
        }
        if (!flag) {
            break;
        }
    }
    pointer = list;
    return pointer;
}

StudentInformation* sort (StudentInformation list[]) {
    StudentInformation* pointer;
    for (int i = 1; i < 4; i++) {
        int e;
        float scoreKey;
        StudentInformation key;
        key = list[i];
        scoreKey = list[i].score;
        e = i - 1;

        while (e >= 0 && list[e].score < scoreKey) {
            list[e + 1] = list[e];
            e -= 1;
        }
        list[e + 1] = key;
    }
    pointer = list;
    return pointer;
}

void printAll(StudentInformation list[]) {
    for (int e = 0; e < 4; e++) {
        print(list[e]);
    }
}

void printAllPointer(StudentInformation* pointer, int size) {
    int e = 0;
    while (e != size) {
        cout << pointer[e].name << endl;
        e++;
    }
}

int main () {
    int e = 0;
    float maxNum;
    StudentInformation student1 = create(20080409, "Ngo The Khang",
                                        "Ho Chi Minh city", 9.5);

    StudentInformation student2 = create(20080822, "Dao Vi Thanh",
                                           "Ho Chi Minh city", 9.6);

    StudentInformation student3 = create(20080625, "Pham Gia Huy",
                                          "Ha Noi", 9.3);

    StudentInformation student4 = create(20080713, "Nguyen Thien An",
                                          "Ho Chi Minh city", 9.8);

    StudentInformation list[4] = {student1, student2, student3, student4};

    cout << endl;
    //printAll (list);
    cout << endl;

    int n = sizeof(list)/sizeof(list[0]);

    StudentInformation *result = selectionSort(list);
    cout << result[0].name << " - " << result[0].score << endl;
    cout << result[1].name << " - " << result[1].score << endl;
    cout << result[2].name << " - " << result[2].score << endl;
    cout << result[3].name << " - " << result[3].score << endl;

    printAllPointer(result, n);

    cout << endl;
    cout << "This is the best student: ";
    print(list[e]);
    return 0;
}