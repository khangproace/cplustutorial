#include <iostream>
using namespace std;

struct Node {
    int data;
    Node *pNext;
};

Node *createNode (int init) {
    Node *node = new Node;
    node->data = init;
    node->pNext = nullptr;
    return node;
};

struct Stack {
    Node *pTop;
};

void createStack (Stack &s) {
    s.pTop = nullptr;
};

bool checkEmpty (Stack s) {
    return s.pTop == nullptr ? true : false;
};

void push (Stack &s, Node *node) {
    if (checkEmpty(s)) {
        s.pTop = node;
    }
    else {
        node->pNext = s.pTop;
        s.pTop = node;
    };
};

int top (Stack &s) {
    if (checkEmpty(s)) {
        return 0;
    }
    return s.pTop->data;
};

int pop (Stack &s) {
    if (checkEmpty(s)) {
        return 0;
    };
    Node *node = s.pTop;
    int data = node->data;
    s.pTop = node->pNext;
    delete node;
    return data;
};

Node *findLargestNode (Node *node1, Node *node2, Node *node3) {
    Node *nodeLargest = createNode(0);
    //sort
    if (node1->data > nodeLargest->data) {
        nodeLargest->data = node1->data;
    }
    if (node2->data > nodeLargest->data) {
        nodeLargest->data = node2->data;
    }
    if (node3->data > nodeLargest->data) {
        nodeLargest->data = node3->data;
    };

    return nodeLargest;
}

void printNodeList (Stack stack) {
    cout << pop(stack);
    cout << pop(stack);
    cout << pop(stack);
}

Node stackSort(Node *node1, Node *node2, Node *node3) {
    Stack stackMain{};
    Stack stackMain2{};
    Stack stackMain3{};

    createStack(stackMain);
    createStack(stackMain2);
    createStack(stackMain3);

    Node *nodeLargest = findLargestNode(node1, node2, node3);

    push(stackMain, node1);
    push(stackMain2, node2);
    push(stackMain3, node3);


    //num1 is largest
    if (node1->data == nodeLargest->data) {
        if (node2->data > node3->data) {
            push(stackMain, node2);
            push(stackMain, node3);
        };
        if (node3->data > node2->data) {
            push(stackMain, node3);
            push(stackMain, node2);
        };
        printNodeList(stackMain);
        cout << endl << "num1 is largest";
    };

    //num2 is largest
    if (node2->data == nodeLargest->data) {
        if (node1->data > node3->data) {
            push(stackMain2, node1);
            push(stackMain2, node3);
        };
        if (node3->data > node1->data) {
            push(stackMain2, node3);
            push(stackMain2, node1);
        };
        printNodeList(stackMain2);
        cout << endl << "num2 is largest";
    };

    //num3 is largest
    if (node3->data == nodeLargest->data) {
        if (node1->data > node2->data) {
            push(stackMain3, node1);
            push(stackMain3, node2);
        };
        if (node2->data > node1->data) {
            push(stackMain3, node2);
            push(stackMain3, node1);
        };
        printNodeList(stackMain3);
        cout << endl << "num3 is largest";
    };
}



int main () {

    Node *node1 = createNode(2);
    Node *node2 = createNode(4);
    Node *node3 = createNode(5);

    stackSort(node1, node2, node3);





    return 0;
}