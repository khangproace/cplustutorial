#include <iostream>
using namespace std;
#define INT_MAX = 100000;

struct Node {
    int data;
    Node* pNext;
};
Node *createNode (int init) {
    Node *node = new Node;
    node->data = init;
    node->pNext = nullptr;
    return node;
};

struct Queue {
    Node *head;
    Node *tail;
};

Queue (createQueue (Queue &s)) {
    s.head = nullptr;
    s.tail = nullptr;
};

bool isEmpty (Queue s) {
    return (s.head == nullptr && s.tail == nullptr);
};

void enQueue (Queue &s, Node *node) {
    if (isEmpty(s)) {
        s.head = node;
        s.tail = node;
    }
    else {
        s.tail->pNext = node;
        s.tail = node;
    }
};

int deQueue (Queue &s) {
    if (isEmpty(s)) {
        return 0;
    };
    Node *node = s.head;
    int data = node->data;
    s.head = node->pNext;
    delete node;
    if (s.head == nullptr) {
        s.tail = nullptr;
    }
    return data;
};

int front (Queue s) {
    return s.head -> data;
};

int minIndex (Queue &s, int sortedIndex) {
    int min_index =
}


