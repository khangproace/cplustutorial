#include <iostream>
using namespace std;

struct StudentInformation {
    float score;
    string studentName;
};

StudentInformation createStudent (string name, float score) {
    StudentInformation student;
    student.studentName = name;
    student.score = score;
    return student;
};

struct Node {
    Node *pNext;
    StudentInformation data;
};

Node *createNode (StudentInformation init) {
    Node *node = new Node;
    node->data = init;
    node->pNext = nullptr;
    return node;
};

struct Queue {
    Node *pHead;
    Node *pTail;
};

Queue createQueue (Queue &s) {
    s.pHead = nullptr;
    s.pTail = nullptr;
};

bool isEmpty (Queue &s) {
    return (s.pHead == nullptr && s.pTail == nullptr)? true : false;
};

void enQueue (Queue &s, Node *node) {
    if (isEmpty(s)) {
        s.pHead = node;
        s.pTail = node;
    };
    s.pTail->pNext = node;
    s.pTail = node;
};

StudentInformation deQueue (Queue &s) {
    if (isEmpty(s)) {
        return createStudent("", 0);
    };
    Node *node = s.pHead;
    StudentInformation data = node->data;
    s.pHead = node->pNext;
    delete node;
    if (s.pHead == nullptr) {
        s.pTail = nullptr;
    };
    return data;
};

StudentInformation top(Queue s) {
    if (isEmpty(s)) {
        return createStudent("", 0);
    };
    return s.pHead->data;
};

int main () {
    float largestScore = 0;
    StudentInformation student1 = createStudent("Ngo The Khang", 8.5);
    StudentInformation student2 = createStudent("Tran Dang Khoa", 9);
    StudentInformation student3 = createStudent("Truong Bao Ha", 7.5);
    StudentInformation student4 = createStudent("Ngo The Tam", 9.5);
    StudentInformation student5 = createStudent("Truong Hoang Long", 6);

    Queue queueSort1;
    Queue queueSort2;
    Queue queueFinal;
    createQueue (queueSort1);
    createQueue (queueSort2);
    createQueue (queueFinal);

    Node *node1 = createNode(student1);
    Node *node2 = createNode(student2);
    Node *node3 = createNode(student3);
    Node *node4 = createNode(student4);
    Node *node5 = createNode(student5);

    enQueue(queueSort1, node1);
    enQueue(queueSort1, node2);
    enQueue(queueSort1, node3);
    enQueue(queueSort1, node4);
    enQueue(queueSort1, node5);



    return 0;
};

