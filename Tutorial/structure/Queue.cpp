#include <iostream>
using namespace std;

struct Node {
    int data;
    Node *pNext;
};

Node *createNode (int init) {
    Node *node = new Node;
    node->data = init;
    node->pNext = nullptr;
    return node;
};

struct Queue {
    Node *head;
    Node *tail;
};

Queue createQueue (Queue &s) {
    s.head = nullptr;
    s.tail = nullptr;
};

bool isEmpty (Queue s) {
    return (s.head == nullptr && s.tail == nullptr);
};

void enQueue (Queue &s, Node *node) {
    if (isEmpty(s)) {
        s.head = node;
        s.tail = node;
    }
    else {
        s.tail->pNext = node;
        s.tail = node;
    }
};

void deQueue (Queue &s) {
    if (isEmpty(s)) {
        return;
    };
    Node *node = s.head;
    int data = node->data;
    s.head = node->pNext;
    delete node;
    if (s.head == nullptr) {
        s.tail = nullptr;
    };
};

int front (Queue s) {
    return s.head->data;
}



int main () {
    Queue queue1;
    createQueue(queue1);

    Node *node1 = createNode(1);
    Node *node2 = createNode(2);
    Node *node3 = createNode(3);


    cout << queue1.head->data;


    return 0;
}