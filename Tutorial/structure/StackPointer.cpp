#include <iostream>
using namespace std;

struct Node {
    float data;
    Node *pNext;
};

Node *createNode (float init) {
    Node *node = new Node;
    node->data = init;
    node->pNext = nullptr;
    return node;
}

struct Stack {
    Node *pTop;
};

void createStack (Stack s) {
    s.pTop = nullptr;
};

bool isEmpty (Stack s) {
    if (s.pTop == nullptr) {
        return true;
    }
    return false;
};

void push(Stack &s, Node *node) {
    if (s.pTop == nullptr) {
        s.pTop = node;
    }
    else {
        node->pNext = s.pTop;
        s.pTop = node;
    }
};

int pop(Stack &s) {
    if (isEmpty(s)) {
        return 0;
    }
    Node *node = s.pTop;
    int data = node->data;
    s.pTop = node->pNext;
    delete node;
    return data;
}

int top(Stack s) {
    if (isEmpty(s)) {
        return 0;
    }
    return s.pTop->data;
}
int main () {
    Stack stack;
    createStack(stack);
    Node *node1 = createNode(1);
    Node *node2 = createNode(2);
    Node *node3 = createNode(3);

    push(stack, node1);
    push(stack, node2);
    push(stack, node3);

    pop(stack);
    cout << top(stack);

    return 0;
}