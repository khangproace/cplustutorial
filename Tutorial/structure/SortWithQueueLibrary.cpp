#include <bits/stdc++.h>
using namespace std;
#define INT_MAX 1000;

//[index]:  0 1 2
//[queue]:  2 6 4

int minIndex (queue <int> &q, int sortedIndex) {
    int min_index = -1;
    int min_val = INT_MAX;
    int n = q.size(); //3
    for (int i = 0; i < n; i++){
        int currentNumber = q.front(); // 4
        q.pop();
        if (currentNumber <= min_val && i <= sortedIndex) {
            min_index = i;
            min_val = currentNumber;
        }
        q.push(currentNumber);
    }
    return min_index;
};

void sortQueue(queue<int> &q) {
    for (int i = 1; i <= q.size(); i++) {
        int min_index = minIndex(q, q.size() - i); //sortedIndex = 1
    }
}

void insertMinToRear (queue <int> &q, int min_index) {
    int min_val;
    int n = q.size();
    for (int i = 0; i < n; i++) {
        int current = q.front();
        q.pop();
        if (i != min_index) {
            q.push(current);
        }
        else {
            min_val = current;
        }
    };
    q.push(min_val);
};

void sortedQueue (queue <int> &q) {
    for (int i = 1; i < q.size(); i++){
        int min_index = minIndex(q, q.size() - i);
        insertMinToRear(q, min_index);
    }
};

int main() {
    queue <int> q;
    q.push(5);
    q.push(15);
    q.push(10);
    q.push(25);
    q.push(20);

    sortedQueue(q);

    while (q.empty() == 0) {
        cout << q.front() << " ";
        q.pop();
    }
    return 0;
}