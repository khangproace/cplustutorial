#include <iostream>
using namespace std;

typedef struct FractionNumber {
    int numerator;
    int denominator;
};

FractionNumber plusFraction(FractionNumber num1, FractionNumber num2) {
    int m1;
    int t1;
    int t2;
    if (num1.denominator != num2.denominator ) {
        t1 = num1.numerator * num2.denominator;
        m1 = num1.denominator * num2.denominator;
        t2 = num2.numerator * num1.denominator;
    }
    int t;
    int m;
    t = t1 + t2;
    m = m1;
    FractionNumber r;
    r.numerator = t;
    r.denominator = m;

    return r;
}
FractionNumber minusFraction(FractionNumber num1, FractionNumber num2) {
    int m1;
    int t1;
    int t2;
    if (num1.denominator != num2.denominator ) {
        t1 = num1.numerator * num2.denominator;
        m1 = num1.denominator * num2.denominator;
        t2 = num2.numerator * num1.denominator;
    }
    int t;
    int m;
    t = t1 - t2;
    FractionNumber r;
    r.numerator = t;
    r.denominator = m1;

    return r;
}
FractionNumber compareNumber(FractionNumber num1, FractionNumber num2) {

    int m1;
    int t1;
    int t2;
    if (num1.denominator != num2.denominator) {
        t1 = num1.numerator * num2.denominator;
        m1 = num1.denominator * num2.denominator;
        t2 = num2.numerator * num1.denominator;
    }
    int t;
    if (t1 > t2) {
        t = t1;
    } else {
        t = t2;
    }
    FractionNumber r;
    r.numerator = t;
    r.denominator = m1;

    return r;
}
FractionNumber multiplyNumber(FractionNumber num1, FractionNumber num2) {
    int result1;
    int result2;

    result1 = num1.numerator * num2.numerator;
    result2 = num1.denominator * num2.denominator;

    FractionNumber r;
    r.numerator = result1;
    r.denominator = result2;
    return r;
}



int main () {

    FractionNumber num1;
    num1.numerator = 2;
    num1.denominator = 5;

    FractionNumber num2;
    num2.numerator = 3;
    num2.denominator = 4;
    FractionNumber number = multiplyNumber(num1, num2);
    cout << number.numerator;
    cout << endl;
    cout << number.denominator;


    return 0;
}
