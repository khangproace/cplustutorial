#include <iostream>
using namespace std;
#define MAX 100000000000000;

struct Node {
    int data;
    Node* next;
};

Node* createNode (int init) {
    Node* node = new Node;
    node->data = init;
    node->next = nullptr;
    return node;
};

struct linkedList {
    Node* head;
    Node* tail;
};

void createList (linkedList& L) {
    L.head = nullptr;
    L.tail = nullptr;
};

void addHead (linkedList& L, Node* node) {
    if (L.head == nullptr) {
        L.head = node;
        L.tail = node;
    }
    else {
        node->next = L.head;
        L.head = node;
    };
};

void addTail (linkedList& L, Node* node) {
    if (L.tail == nullptr) {
        L.head = node;
        L.tail = node;
    }
    else{
        L.tail->next = node;
        L.tail = node;
    };
};

void addMiddle (linkedList& L, Node* f, Node* b) {
    if (f != nullptr) {
        b->next = f->next;
        f->next = b;
        if (L.tail == f) {
            L.tail = b;
        };
    }
    else {
        addHead(L, b);
    };
};

void removeHead (linkedList& L) {
    if (L.head != nullptr) {
        Node* node = L.head;
        L.head = node->next;
        cout << endl << "*Remove " << node->data << " success*";
        delete node;
        if (L.head == nullptr) {
            L.tail = nullptr;
        };
    };
    if (L.head == nullptr) {
        cout << endl << "*Nothing to remove*";
    };
};

void removeAfter (linkedList& L, Node *f) {
    if (f != nullptr) {
        Node* b = f->next;
        if (b != nullptr) {
            if (L.tail == b) {
                L.tail = f;
            };
            f->next = b->next;
            delete b;
            cout << endl << "*Remove success*";
        };
        cout << endl << "*Remove success*";
    };
    cout << endl << "*Cannot remove*";
};


void size (linkedList& L) {
    int count = 0;
    if (L.head != nullptr) {
        for (Node* node = L.head; node != nullptr; node = node->next) {
            count += 1;
        };
        cout << count;
    };
};

void printAllNode (linkedList& l) {
    Node* node;
    for (node = l.head; node != nullptr; node = node->next) {
        cout << node->data << " ";
    };
};

Node* findNode (linkedList& l, int x) {
    Node* node;
    for (node = l.head; node != nullptr; node = node->next) {
        if (node->data == x) {
            break;
        }
    };
    if (node != nullptr) {
        return node;
    };
    return nullptr;
};

void deleteList (linkedList& l) {
    Node* node;
    for (node = l.head; node != nullptr; node = l.head) {
        removeHead(l);
    };
    l.tail = nullptr;
}

int main() {

    linkedList List1;
    createList(List1);
    Node* node1 = createNode(1);
    Node* node2 = createNode(2);
    Node* node3 = createNode(3);
    Node* node4 = createNode(4);
    Node* node5 = createNode(5);

    addHead(List1, node1);
    addHead(List1, node2);
    addHead(List1, node3);
    addHead(List1, node4);
    addHead(List1, node5);

    size (List1);

    deleteList(List1);

    printAllNode(List1);



    return 0;
}