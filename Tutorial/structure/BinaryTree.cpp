#include <iostream>
using namespace std;

int summ = 0;

struct Node {
    int data;
    Node *right;
    Node *left;
};

Node *createNode (int init) {
    Node *node = new Node;
    node->data = init;
    node->right = nullptr;
    node->left = nullptr;
    return node;
};

typedef Node* Tree;
Tree myTree;
void createTree (Tree &root) {
    root = nullptr;
};

void NLR (Tree root) {
    if (root) {
        cout << root->data << " ";
        NLR (root->left);
        NLR (root->right);
    };
};

void LNR (Tree root) {
    if (root) {
        LNR (root->left);
        cout << root->data << " ";
        LNR (root->right);
    };
};

void LRN (Tree root) {
    if (root) {
        LRN (root->left);
        LRN (root->right);
        cout << root->data << " ";
    };
};

int sum (Tree root) {
    if (root) {
        summ += 1;
        sum (root->left);
        sum (root-> right);
    }
    return summ;
};

void deleteTree (Tree root) {
    if (root) {
        sum (root->left);
        sum (root->right);
        delete root;
    };
};

void addNode (Tree &root, Node *node) {
    if (root) {
        if (node->data == root->data) {
            return;
        };
        if (node->data < root->data) {
            addNode(root->left, node);
        }
        else {
            addNode(root->right, node);
        };
    }
    else {
        root = node;
    };
};

Node *findNode (Tree &root, int x) {
    Node *searchNode = nullptr;
    if (root) {
        if (x == root->data) {
            return root;
        };
        if (x < root->data) {
            searchNode = findNode(root->left, x);
            if (searchNode == nullptr) {
                searchNode = findNode(root->right, x);
            }
        } else {
            searchNode = findNode(root->right, x);
            if (searchNode == nullptr) {
                searchNode = findNode(root->left, x);
            }
        }
    }
    return searchNode;
};