#include <iostream>
#define MAX 10000
#include <unistd.h>
using namespace std;

int main () {
    int n;
    int numArray[MAX];
    int i = 0;
    int e = 0;
    int t = 0;

    bool flag = false;

    const string STR_NO_SORT = " this one is not sorted";
    const string STR_SORT = " this one is sorted";

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    while (i < n) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
        i += 1;
    }
    while (e < n - 1) {
        if (numArray[e] > numArray[e + 1]) {
            t = numArray[e];
            numArray[e] = numArray[e + 1];
            numArray[e + 1] = t;
            cout << numArray[e] << " " << numArray[e + 1] << " " << endl;
            sleep (2);
            flag = true ;
        }
        e += 1;
        if (flag) {
            e = 0;
            flag = false;
        }
    }

    e = 0;
    while (e < n) {
        cout << numArray[e] << " ";
        e += 1;
    }
    return 0;
}