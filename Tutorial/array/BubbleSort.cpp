#include <iostream>
#define MAX 10000
using namespace std;

int main () {
    int  numArray[MAX];
    int e = 0;
    int g;
    bool flag = false;

    numArray[0] = 12;
    numArray[1] = 11;
    numArray[2] = 13;
    numArray[3] = 5;
    numArray[4] = 6;

    while (e < 5 - 1) {
        if (numArray[e] > numArray[e + 1]) {
            g = numArray[e];
            numArray[e] = numArray[e + 1];
            numArray[e + 1] = g;
            flag = true ;
        }
        e += 1;
        if (flag) {
            e = 0;
            flag = false;
        }
    }

    for (e = 0; e < 5; e++) {
        cout << numArray[e] << " ";
    }
    return 0;
}