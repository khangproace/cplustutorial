#include <iostream>
#define MAX 10000
using namespace std;

int main () {
    int n;
    int numArray[MAX];
    int i = 0;
    int e = 0;
    int c = 0;
    int r = 0;
    int g = 0;

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    while (i < n) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
        i += 1;
    }

    //1
    while (e < n) {
        c += numArray[e] ;
        e += 1;
    }
    cout << c << endl;


    //2
    e = 0;
    while (e < n) {
        if (numArray[e] > 0 && numArray[e] % 2 == 0){
            r += numArray[e];
        }
        e += 1;
    }
    cout << r << endl;

    //
    e = 0;
    while (e < n) {
        if (numArray[e] > 9) {
            int b = numArray[e] / 10;
            if (b % 2 != 0) {
                g += b;
            }
        }
        else {
            if (numArray[e] % 2 != 0){
                g += numArray[e];
            }
        }
        e += 1;
    }
    cout << g;
    return 0;
}


