#include <iostream>
using namespace std;

int main () {
    int e;
    int min;
    int j;
    int numArray[5] = {3,1,4,2,5};

    for (e = 0; e < 5 - 1; e++) {
        min = e + 1;
        for (j = e + 1; j < 5; j++) {
            if (numArray[min] > numArray[j]) {
                min = j;
            }
        }
        if (numArray[e] > numArray[min]) {
            int t = numArray[e];
            numArray[e] = numArray[min];
            numArray[min] = t;
        }
    }
    cout << endl;
    for (e = 0; e < 5; e++) {
        cout << numArray[e] << " ";
    }
    return 0;
}