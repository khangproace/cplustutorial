#include <iostream>
#include <dos.h>
#include <unistd.h>

#define MAX 10000
using namespace std;

int main () {
    int n;
    int numArray[MAX];
    int i = 0;
    int e = 0;
    int s = 0;
    int t = 0;

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    while (i < n) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
        i += 1;
    }
    while (e < n) {
        cout << numArray[e] << endl;
        e += 1;
    }

    cout << "Wait..." << endl;
    for (e = 0; e < n; e++) {
        cout << numArray[e] << " ";
    }
    cout << endl;

    for (e = 0; e < n - 1; e++) {
        t = numArray[e];
        numArray[e] = numArray[e + 1];
        numArray[e + 1] = t;
        for (s = 0; s < n; s++) {
            cout << numArray[s] << " ";
        }
        cout << endl;
        sleep(2);
    }
    return 0;
}
