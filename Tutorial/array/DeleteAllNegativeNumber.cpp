#include <iostream>
#define MAX 10000
using namespace std;

int main () {
    int n;
    int numArray[MAX];
    int i = 0;
    int e = 0;
    int s = 0;
    int c = 0;
    bool flag = true;

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    for (i = 0; i < n; i++) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
    }

    for (e = 0; e < n; e++) {
        cout << numArray[e] << " ";
    }

    cout << endl;

    while(flag) {
        flag = false;
        for (e = 0; e < n; e++) {
            if (numArray[e] < 0) {
                for (int u = e; u < n; u++) {
                    s = numArray[u];
                    numArray[u] = numArray[u + 1];
                    numArray[u + 1] = s;
                }
                flag = true;
                n -= 1;
            }
        }
    }

    for (e = 0; e < n; e++) {
        cout << numArray[e] << " ";
    }

    return 0;
}