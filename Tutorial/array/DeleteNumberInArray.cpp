#include <iostream>
#define MAX 10000
using namespace std;

int main () {
    int n;
    int numArray[MAX];
    int i = 0;
    int e = 0;
    int s = 0;
    int t = 0;

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    while (i < n) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
        i += 1;
    }

    while (e < n) {
        cout << numArray[e] << endl;
        e += 1;
    }

    cout << "Enter the index number you want to delete: ";
    cin >> s;

    while (s < n) {
        t = numArray[s];
        numArray[s] = numArray[s + 1];
        numArray[s + 1] = t;
        s += 1;
    }
    n -= 1;
    e = 0;
    while (e < n) {
        cout << numArray[e] << endl;
        e += 1;
    }

    return 0;
}