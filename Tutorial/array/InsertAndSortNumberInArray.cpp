#include <iostream>
#define MAX 10000
using namespace std;

int main () {
    int n;
    int numArray[MAX];
    int i = 0;
    int e = 0;
    int s = 0;
    int c = 0;
    int t = 0;
    bool flag = true;

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    for (i = 0; i < n; i++) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
    }

    for (e = 0; e < n; e++) {
        cout << numArray[e] << " ";
    }

    cout << endl;
    cout << "Enter your number: ";
    cin >> s;
    cout << "Enter your index: ";
    cin >> c;

    for (e = n; e >= c; e--) {
        numArray[e] = numArray[e - 1];
    }
    numArray[e] = s;

    for (e = 0; e < n + 1; e++) {
        cout << numArray[e] << " ";
    }

    while (flag) {
        flag = false;
        for (e = 0; e < n; e++) {
            if (numArray[e] > numArray[e + 1]) {
                t = numArray[e];
                numArray[e] = numArray[e + 1];
                numArray[e + 1] = t;
                flag = true;
            }
        }
    }

    cout << endl;
    for (e = 0; e < n + 1; e++) {
        cout << numArray[e] << " ";
    }


    return 0;
}