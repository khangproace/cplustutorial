#include <iostream>
#define MAX 10000
using namespace std;

int main () {
    int n;
    int numArray[MAX];
    int i = 0;
    int e = 0;
    int count = 0;

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    while (i < n) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
        i += 1;
    }

    while (e < n) {
        cout << numArray[e] << " ";
        e += 1;
    }

    e = 0;
    while (e < n) {
        if (numArray[e] % 2 != 0) {
            count += 1;
        }
        e += 1;
    }
    if (count >= 1) {
        cout << " <=== This array have odd numbers.";
    }
    else {
        cout << " <=== This array is full of even numbers.";
    }
    return 0;
}
