#include <iostream>
#define MAX 10000
using namespace std;

int main() {
    int n = 1;
    int endIndex1 = 0;
    int startIndex = 0;
    int endIndex2 = 0;
    int numArray[MAX];
    int r = 0;
    int i = 0;

    cout << "Please enter your value: ";
    cin >> endIndex1;
    while (n <= endIndex1) {
        numArray[n - 1] = n;
        n += 1;
    }
    while (i < n) {
        cout << numArray[i] << endl;
        i += 1;
    }

    cout << "Please enter your start index: ";
    cin >> startIndex;
    cout << "Please enter your end index: ";
    cin >> endIndex2;

    r = numArray[startIndex];
    while (r <= endIndex2) {
        cout << r << endl;
        r += 1;
    }
    return 0;
}