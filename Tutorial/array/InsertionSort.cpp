#include <iostream>
using namespace std;

int main() {
    int numArray[5] = {5,3,1,4,2};
    int n = 5;
    int e = 0;
    int i;
    int key;

    for (i = 1; i < 5; i++) {
        key = numArray[i];
        e = i - 1;
        while (e >= 0 && numArray[e] > key) {
            numArray[e + 1] = numArray[e];
            e -= 1;
        }
        numArray[e + 1] = key;
    }
    for (int g = 0; g < 5; g++) {
        cout << numArray[g] << " ";
    }


    return 0;
}
