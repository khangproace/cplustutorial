#include <iostream>
#define MAX 100000
using namespace std;

int main () {
    int n;
    int numArray[MAX];
    int i = 0;
    int e = 0;
    int s = 0;
    int t = 0;

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    while (i < n) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
        i += 1;
    }
    while (e < n) {
        cout << numArray[e] << endl;
        e += 1;
    }

    e = 0;
    while (e < n) {
        if (numArray[e] == 0) {
            s = e;
        }
        e += 1;
    }

    cout << "Location of zero is " << s << endl;

    t = numArray[s];
    numArray[s] = numArray[n / 2];
    numArray[n / 2] = t;

    e = 0;
    while (e < n) {
        cout << numArray[e] << endl;
        e += 1;
    }
    return 0;
}