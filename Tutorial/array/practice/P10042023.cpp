//1. Tao 1 user co cac thong tin sau: Ho ten, tai khoan, mat khau da duoc ma hoa
//2. Xay dung ham xuat ra thong tin nguoi dung
//3. Tao 1 danh sach user sap xep theo thu tu alphabet (Dung thuat toan bubble and insertion)
//4. Tao 1 danh sach nguoi dung bang pointer

#include <iostream>
using namespace std;

struct UserInformation  {
    string name, username, password;
};

UserInformation create (string name, string username, string password) {
    UserInformation user;
    user.name = name;
    user.username = username;
    user.password = password;
    return user;
};

void print (const UserInformation &user) {
    cout << "Name: " << user.name << " |" << " ";
    cout << "Username: "<< user.username << " |" <<" ";
    cout << "Password: " <<  user.password << " |" << " ";
    cout << endl;
};

void printAll (UserInformation array[]) {
    for (int e = 0; e < 3; e++) {
        print (array[e]);
    }
};

UserInformation* sort (UserInformation array[]) {
    string a = array[0].name;
    string b = array[1].name;
    string c = array[2].name;
    int t;
    int e = 0;
    int sortArray[3] = {int(a[0]), int(b[0]), int(c[0])};
    bool flag = false;
    while (e < 2) {
        if (sortArray[e] < sortArray[e + 1]) {
            t = sortArray[e];
            sortArray[e] = sortArray[e + 1];
            sortArray[e + 1] = t;
            flag = true;
        }
        e += 1;
        if (flag) {
            e = 0;
            flag = false;
        }
    }

    for (e = 0; e < 3; e++) {
        if ((sortArray[e] = int(a[0]))) {
            print(array[0]);
        }
        else if ((sortArray[e] = int(b[1]))) {
            print(array[1]);
        }
        else if ((sortArray[e] = int(c[1]))) {
            print(array[2]);
        }
        else {
            cout << "Error";
        }
    }

}
int main () {
    UserInformation array[3];
    UserInformation user1 = create("Khang", "KhangNgo", "09042008");
    UserInformation user2 = create("Khoa", "KhoaTran", "123456");
    UserInformation user3 = create("Quyen", "QuyenNguyen", "98765");

    array[0] = user1;
    array[1] = user2;
    array[2] = user3;

    printAll(array);

    sort(array);


    return 0;
}
