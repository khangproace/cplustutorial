#include <iostream>
using namespace std;

typedef struct Car {
    string length;
    string height;
    double price;
    int year;
    string name;
    string brand;
    string typeOfCar;
    int seats;
    string color;
    string drivetrain;
    string weight;
    float horsepower;
    float torque;
    string engine;
    float acceleration;
    string fuelEconomy;
    string gasTankVolume;
    string petrol;
    int maxSpeed;
    string transmission;
};

Car create(string length, string height, double price, int year,string name,
           string brand, string typeOfCar, int seats,string color,string drivetrain,
           string weight, float horsepower, float torque, string engine, float acceleration, string fuelEconomy,
           string gasTankVolume, string petrol, int maxSpeed, string transmission) {
    Car info;
    info.length = length;
    info.height = height;
    info.price = price;
    info.year = year;
    info.name = name;
    info.brand = brand;
    info.typeOfCar = typeOfCar;
    info.seats = seats;
    info.color = color;
    info.drivetrain = drivetrain;
    info.weight = weight;
    info.horsepower = horsepower;
    info.torque = torque;
    info.engine = engine;
    info.acceleration = acceleration;
    info.fuelEconomy = fuelEconomy;
    info.gasTankVolume = gasTankVolume;
    info.petrol = petrol;
    info.maxSpeed = maxSpeed;
    info.transmission = transmission;

    return info;
}

void print(Car info) {
    cout << "Brand: " << info.brand << endl;
    cout << "Model: " << info.name << endl;
    cout << "Year: " << info.year << endl;
    cout << "Type: " << info.typeOfCar << endl;
    cout << "                    : " << info.seats << endl;
    cout << "Color: " << info.color << endl;
    cout << "Length: " << info.length << endl;
    cout << "Height : " << info.height<< endl;
    cout << "Weight: " << info.weight << endl;
    cout << "Drivetrain: " << info.drivetrain << endl;
    cout << "Fuel: " << info.petrol << endl;
    cout << "Tank volume: " << info.gasTankVolume << endl;
    cout << "Transmission: " << info.transmission << endl;
    cout << "Engine: " << info.engine << endl;
    cout << "Fuel economy: " << info.fuelEconomy << endl;
    cout << "Horsepower: " << info.horsepower << " hp" << endl;
    cout << "Torque: " << info.torque << " Nm"  <<endl;
    cout << "Max speed: " << info.maxSpeed << " km/h" << endl;
    cout << "Acceleration : " << info.acceleration << " Seconds" << endl;
    cout << "Price: " << info.price << "$" <<  endl ;
    cout << "*************************************************" << endl;
}

void compare(Car info1, Car info2) {
    if (info1.horsepower < info2. horsepower) {
        cout << info1.name << " have less horsepower than " << info2.name;
    }
}

Car copyFrom(Car info1) {
    return create(info1.length, info1.height, info1.price, info1.year, info1.name, info1.brand, info1.typeOfCar, info1.seats, info1.color, info1.drivetrain,
           info1.weight, info1.horsepower, info1.torque, info1.engine, info1.acceleration, info1.fuelEconomy, info1.gasTankVolume,
           info1.petrol, info1.maxSpeed, info1.transmission);
}

Car* bubbleSortBySeats (Car list[]) {
    Car* pointer;
    int e;
    int j;
    Car t;
    bool flag;

    for (e = 0; e < 2 - 1; e++) {
        flag = false;
        for (j = 0; j < 2 - e - 1; j++) {
            if (list[j].seats < list[j + 1].seats) {
                t = list[j];
                list[j] = list[j + 1];
                list[j + 1] = t;
            }
        }
        if (!flag) {
            break;
        }
    }
    pointer = list;
    return pointer;
}

int main () {

    Car info1 = create("4963 mm", "1498 mm", 56000, 2020, "Series 5 520i G30",
                      "BMW", "Sedan | Station wagon", 5, "Carbon Black, Alpine White, Bluestone Metallic and Phytonic Blue",
                      "RWD", "2350 kg", 184, 290, "2.0l inline 4 | Twin turbocharged", 7.8,
                      "5.8l ~ 6.1l/100km", "68 litres", "Petrol",  235, "Automatic (Manumatic), 8 - speed");

    Car info2 = create ("4,983 mm", "1,469 mm", 144000, 2021, "Series 5 M Competition Steptronic", "BMW", "Sport Sedan | Sport car", 4,
                        " Individual Frozen Cashmere Silver Metallic, Individual Frozen Dark Silver, Tanzanite Blue, Alvit Grey Metallic,"
                        " Frozen Bluestone Metallic, Motegi Red Metallic, Snapper Rocks Blue Metallic, Individual Brilliant White Metallic,"
                        " Individual Imola Red, Aventurine Red Metallic, Individual Frozen Brilliant White Metallic, Marina Bay Blue Metallic, "
                        "Black Sapphire Metallic, Alpine White, Inidvidual Pure Metal Silver, Bluestone Metallic, Donington Grey Metallic, "
                        "Individual Frozen Marina Bay Blue Metallic and Brands Hatch Grey metallic", "AWD", "2,440 kg", 625, 750,
                        "4.4l V8 | Twin turbocharged", 3.3, "11.1 - 11.3 l/100km", "68 litres", "Petrol",
                        305, "Automatic (Manumatic), 8 - speed");

    Car list[2] = {info1, info2};

    Car info3 = copyFrom(info2);

    print(info1);
    print (info2);

    Car* carPointer = bubbleSortBySeats(list);
    cout << carPointer[0].name << endl;
    cout << "************************";
    cout << endl;

    compare(info1, info2);
    return 0;
}