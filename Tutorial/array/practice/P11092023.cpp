#include <iostream>
using namespace std;

int main () {
    int numArray[5];
    int e;
    int t = 0;
    int j;
    bool flag;

    numArray[0] = 3;
    numArray[1] = 1;
    numArray[2] = 2;
    numArray[3] = 5;
    numArray[4] = 4;

    for (e = 0; e < 5 - 1; e++) {
        flag = false;
        for (j = 0; j < 5 - e - 1; j++) {
            if (numArray[j] > numArray[j + 1]) {
                t = numArray[j + 1];
                numArray[j + 1] = numArray[j];
                numArray[j] = t;
                flag = true;
            }
        }
        if (!flag) {
            break;
        }
    }

    e = 0;
    while (e < 5) {
        cout << numArray[e] << endl;
        e += 1;
    }


    return 0;
}