#include <iostream>
#define MAX 10000
using namespace std;

int main () {
    int n;
    int numArray[MAX];
    int i = 0;
    int e = 0;
    int negative = 0;

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    while (i < n) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
        i += 1;
    }
    while (e < n) {
        if (numArray[e] / 1 < 0) {
            negative += 1;
        }
        e += 1;
    }
    cout << "You have " << negative << " negative number";
    return 0;
}