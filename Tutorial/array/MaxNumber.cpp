#include <iostream>
#define MAX 10000

using namespace std;

int main () {

    int n;
    int numArray[MAX];
    int i = 0;
    int e = 1;

    int maxNum;

    cout << "Enter your n: ";
    cin >> n;
    cout << "Your n is: " << n << endl;
    while (i < n) {
        cout << "num" << "[" << i + 1 << "]: ";
        cin >> numArray[i];
        i += 1;
    }

    maxNum = numArray[0];
    while (e < n) {
        if (maxNum < numArray[e]) {
            maxNum = numArray[e];
        }
        e += 1;
    }
    cout << "your largest number is: " << maxNum << endl;

    return 0;
}
